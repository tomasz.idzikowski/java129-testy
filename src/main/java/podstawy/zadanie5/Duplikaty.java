package podstawy.zadanie5;

import java.util.*;

public class Duplikaty {
    public static List<String> removeDuplicates(List<String> input) {
        Set<String> stringSet = new LinkedHashSet<>(input);
        List<String> result=new ArrayList<>();
        stringSet.stream().forEach(s->result.add(s));
        return result;
    }
}
