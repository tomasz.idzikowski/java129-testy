package podstawy.zadanie6;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class Generator {

    public static Set<Integer> generateNumbers(int x,int a, int b){
        Set<Integer> result=new TreeSet<>();
        Random random=new Random();
        while (result.size()!=x){
            result.add(random.nextInt(b-a+1)+a);
        }
        return result;
    }
}
