package podstawy.zadanie8;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class UserManager {
    private List<User> users = new ArrayList<>();

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public List<User> getAdmins() {
        return users.stream()
                .filter(User::isAdmin)
                .collect(Collectors.toList());
    }
}

