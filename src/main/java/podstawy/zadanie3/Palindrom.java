package podstawy.zadanie3;

public class Palindrom {
    public static boolean czyPalindrom(String text) {
        String reversed = new StringBuilder(text).reverse().toString();
        if (reversed.equals(text)){
            return true;
        }
        return false;
    }
}
