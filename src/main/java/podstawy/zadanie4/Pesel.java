package podstawy.zadanie4;

import java.util.regex.Pattern;

public class Pesel {
    public static boolean czyPoprawnyPesel(String pesel) {
        if (Pattern.matches("[0-9]{11}", pesel)) {
            int cyfra1 = Integer.parseInt(pesel.charAt(0) + "");
            int cyfra2 = Integer.parseInt(pesel.charAt(1) + "");
            int cyfra3 = Integer.parseInt(pesel.charAt(2) + "");
            int cyfra4 = Integer.parseInt(pesel.charAt(3) + "");
            int cyfra5 = Integer.parseInt(pesel.charAt(4) + "");
            int cyfra6 = Integer.parseInt(pesel.charAt(5) + "");
            int cyfra7 = Integer.parseInt(pesel.charAt(6) + "");
            int cyfra8 = Integer.parseInt(pesel.charAt(7) + "");
            int cyfra9 = Integer.parseInt(pesel.charAt(8) + "");
            int cyfra10 = Integer.parseInt(pesel.charAt(9) + "");
            int suma = cyfra1 + cyfra2 * 3 + cyfra3 * 7 + cyfra4 * 9 + cyfra5 + cyfra6 * 3 + cyfra7 * 7 + cyfra8 * 9 + cyfra9 + cyfra10 * 3;
            int liczba = 10 - suma % 10;
            if (liczba == 10) {
                liczba = 0;
            }
            return (liczba == Integer.parseInt(pesel.charAt(10) + ""));
        }
        return false;
    }
}
